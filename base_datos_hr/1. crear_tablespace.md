# Solucion SQL Developer

```sh
SQL> show parameter local_listener
SQL> alter system set LOCAL_LISTENER='(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))' scope=both;
SQL> alter system register;
SQL> show parameter local_listener
```

# Crear tablespace

```sql
create tablespace DESARROLLO
  datafile '/opt/oracle/oradata/XE/XEPDB1/desarrollo01.dbf'
  size 100M
  autoentend on next 10 M
  maxsize 2G;

Podemos agregar datafiles a un espacio de tablas por medio del comado alter tablespace:

alter tablespace DESARROLLO
  add datafile '/opt/oracle/oradata/XE/XEPDB1/desarrollo02.dbf'
  size 60M
  autoentend on next 10 M
  maxsize 1G;

Hasta el momento el espacio de tabla DESARROLLO contiene 2 archivos de datos con una capacidad de almacenamiento de 3GB que se contabilizan dentro del límite de los 12GB de capacidad máxima de almacenamiento de datos de usuario permitidos en 18c XE .  

Para eliminar espacios de tabla y su contenido se dispone del  comando siguiente:

drop tablespace DESARROLLO including contents and datafiles; 

Si se necesita eliminar un datafile, emplear el siguiente comando:

alter tablespace DESARROLLO drop datafile  '/opt/oracle/oradata/XE/XEPDB1/desarrollo02.dbf'

Este fue una breve introducción a la creación y administración de tablespaces y su importancia en la organización física de la base de datos.
```