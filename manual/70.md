# 70.  Agregar campos y restricciones (alter table)

Podemos agregar un campo a una tabla y en el mismo momento aplicarle una restricción.
Para agregar un campo y establecer una restricción, la sintaxis básica es la siguiente:

```sql
alter table TABLA
add CAMPO DEFINICION
constraint NOMBRERESTRICCION TIPO;
```

Agregamos a la tabla "libros", el campo "titulo" de tipo varchar2(30) y una restricción "unique":

```sql
alter table libros
add titulo varchar2(30)
constraint UQ_libros_autor unique;
```

Agregamos a la tabla "libros", el campo "codigo" de tipo number(4) not null y una restricción "primary key":

```sql
alter table libros
add codigo number(4) not null
constraint PK_libros_codigo primary key;
```

Agregamos a la tabla "libros", el campo "precio" de tipo number(6,2) y una restricción "check":

```sql
alter table libros
add precio number(6,2)
constraint CK_libros_precio check (precio>=0);
```

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.
Eliminamos la tabla:

```sql
 drop table libros;
```

Creamos la tabla con la siguiente estructura:

```sql
 create table libros(
  autor varchar2(30),
  editorial varchar2(15)
 );
```

Agregamos el campo "titulo" de tipo varchar2(30) y una restricción "unique":

```sql
alter table libros
add titulo varchar2(30)
constraint UQ_libros_autor unique;
```

Veamos si la estructura cambió:

```sql
describe libros;
```

Agregamos el campo "codigo" de tipo number(4) not null y en la misma sentencia una restricción "primary key":

```sql
alter table libros
add codigo number(4) not null
constraint PK_libros_codigo primary key;
```
  
Agregamos el campo "precio" de tipo number(6,2) y una restricción "check" que no permita valores negativos para dicho campo:

```sql
alter table libros
add precio number(6,2)
constraint CK_libros_precio check (precio>=0);
```

Veamos la estructura de la tabla y las restricciones:

```sql
describe libros;
select *from user_constraints where table_name='LIBROS';
```

La tabla contiene 5 campos y 4 restricciones.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    autor varchar2(30),
    editorial varchar2(15)
);

 -- Agregamos el campo "titulo" de tipo varchar2(30) y una restricción "unique":
alter table libros
add titulo varchar2(30)
constraint UQ_libros_autor unique;

 -- Veamos si la estructura cambió:
describe libros;

 -- Agregamos el campo "codigo" de tipo number(4) not null y en la misma
 -- sentencia una restricción "primary key":
alter table libros
add codigo number(4) not null
constraint PK_libros_codigo primary key;

 -- Agregamos el campo "precio" de tipo number(6,2) y una restricción
 -- "check" que no permita valores negativos para dicho campo:
alter table libros
add precio number(6,2)
constraint CK_libros_precio check (precio>=0);

 -- Veamos la estructura de la tabla y las restricciones:
describe libros;

select *from user_constraints where table_name='LIBROS';
```

## Ejercicios propuestos

Trabaje con una tabla llamada "empleados".
1. Elimine la tabla y créela:

```sql
drop table empleados;

create table empleados(
    documento char(8) not null,
    nombre varchar2(10),
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Buenos Aires'
);
```

2. Agregue el campo "legajo" de tipo number(3) y una restricción "primary key"

3. Vea si la estructura cambió y si se agregó la restricción

4. Agregue el campo "hijos" de tipo number(2) y en la misma sentencia una restricción "check" que no permita valores superiores a 30

5. Ingrese algunos registros:

```sql
insert into empleados values('22222222','Juan Lopez','Colon 123','Cordoba',100,2);
insert into empleados values('23333333','Ana Garcia','Sucre 435','Cordoba',200,3);
```

6. Intente agregar el campo "sueldo" de tipo number(6,2) no nulo y una restricción "check" que no permita valores negativos para dicho campo.
No lo permite porque no damos un valor por defecto para dicho campo no nulo y los registros existentes necesitan cargar un valor.

7. Agregue el campo "sueldo" de tipo number(6,2) no nulo, con el valor por defecto 0 y una restricción "check" que no permita valores negativos para dicho campo

8. Recupere los registros

```sql
select *from empleados;
```

9. Vea la nueva estructura de la tabla

10. Vea las restricciones
